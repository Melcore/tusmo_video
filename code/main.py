from find_colors import find_colors
from compute_word import bests_words

target = "JOURNEE"
word = ""
red, yellow, blue = [(0, "J")], [], []


import warnings
warnings.filterwarnings("ignore", 'This pattern is interpreted as a regular expression, and has match groups.')

while not word == target:
    word = str(bests_words(7, red, yellow, blue)[0])
    print(word)
    colors = find_colors(target, word)
    print(colors)

    for i, color in enumerate(colors):
        if color == "R":
            red.append((i, word[i]))
        elif color == "Y":
            yellow.append((i, word[i]))
        else:
            blue.append((i, word[i]))

print("Trouvé")

# TODO interdire la possibilité de mettre deux foix le meme mot
