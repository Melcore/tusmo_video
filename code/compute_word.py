import pandas

dico = pandas.read_csv("dico_fr/Lexique381.csv", sep="\t")
dico = dico[dico["1_ortho"].str.len() > 5]
dico = dico[dico["1_ortho"].str.len() < 11]
dico = dico[~dico["1_ortho"].str.contains(" ")]
dico = dico[~dico["1_ortho"].str.contains("-")]
dico["1_ortho"] = dico["1_ortho"].str.upper()
dico["1_ortho"] = dico["1_ortho"].str.normalize("NFKD").str.encode('ascii', errors='ignore').str.decode('utf-8')
dico["3_lemme"] = dico["3_lemme"].str.normalize("NFKD").str.encode('ascii', errors='ignore').str.decode('utf-8')


lexique = pandas.Series(dico["1_ortho"].unique())
base_lexique = list(dico["3_lemme"].unique())



def bests_words(wordsize, red, yellow, blue):
    ss_lexique = lexique[lexique.str.len() == wordsize]
    for pos, car in red:
        ss_lexique = ss_lexique[ss_lexique.str[pos] == car]
    for pos, car in yellow:
        mask1 = ss_lexique.str[pos] != car
        mask2 = ss_lexique.str.contains(car)
        ss_lexique = ss_lexique[mask1 & mask2]
    for car in blue:
        ss_lexique = ss_lexique[~ss_lexique.str.contains(str(car))]
    return ss_lexique.reset_index(drop=True)
## Attention au doublons jaune - rouge



if __name__=="__main__":
    red = [(0, 'C')]
    yellow = [(1,'O'), (2, 'R'), (4, 'O'), (5, 'N'), (7, 'T'), (8, 'E')]
    blue = [(0, 'A'), (1, 'B'), (2, 'A'), (3, 'I'), (4, 'S'), (5, 'S'), (6, 'A')]

    print(bests_words(9, red, yellow, blue))




