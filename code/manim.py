from manim import *

class Motus(Scene):
    def construct(self):
        rows = 6
        cols = 7
        square_size = 1.0

        result_word = "BALLONS" 
        placing_words = ["BOURSES", "BATEAUX"]
        add_word = "BALLOTS"

        yellow = "#5A22D4"
        red = "#0AD132"
        self.camera.background_color = "#06001e"

        grid = self.create_grid(rows, cols, square_size)
        self.add(grid)

        for row, word in enumerate(placing_words):
            for col, letter in enumerate(word):
                letter_text = Text(letter, color=WHITE, font="Fira Sans")
                letter_text.move_to(grid[row][col].get_center())

                if result_word[col] == letter:
                    red_square = grid[row][col]
                    red_square.set_fill(red, opacity=1)
                
                elif letter in result_word:
                    yellow_circle = Circle(radius=square_size/2-0.05, fill_opacity=1, color=yellow)
                    yellow_circle.move_to(grid[row][col].get_center())
                    self.add(yellow_circle)

                self.add(letter_text)

        actual_word = VGroup()
        for col in range(cols):
            letter_text = Text("." if col != 0 else result_word[0], color=WHITE, font="Fira Sans")
            letter_text.move_to(grid[len(placing_words)][col].get_center())
            actual_word.add(letter_text)

        self.add(actual_word)

        # word input
            
        for col, letter in enumerate(add_word):
            letter_text = Text(letter, color=WHITE, font="Fira Sans")
            letter_text.move_to(grid[len(placing_words)][col].get_center())
            self.play(Transform(actual_word[col], letter_text), run_time=0.2)

        # word validation
        for col, letter in enumerate(add_word):
            letter_text = Text(letter, color=WHITE, font="Fira Sans")
            letter_text.move_to(grid[len(placing_words)][col].get_center())

            if result_word[col] == letter:
                    red_square = grid[len(placing_words)][col]
                    red_square.set_fill(red, opacity=1)
                    self.add_sound("red.mp3")
                
            elif letter in result_word:
                yellow_circle = Circle(radius=square_size/2-0.05, fill_opacity=1, color=yellow)
                yellow_circle.move_to(grid[len(placing_words)][col].get_center())
                self.add(yellow_circle)
                self.add_sound("yellow.mp3")
            else:
                self.add_sound("neutre.mp3")


            self.add(letter_text)
            self.wait(0.4)
        
        self.wait(1)


            
            
        

        


    def create_grid(self, rows, cols, square_size):
        grid = VGroup()

        for i in range(rows):
            row = VGroup()
            for j in range(cols):
                square = Square(side_length=square_size, fill_opacity=0, color=WHITE)
                square.move_to(square_size * (j * RIGHT - i * UP))
                row.add(square)
            grid.add(row)

        grid.move_to(ORIGIN)
        return grid