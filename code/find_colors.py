def find_colors(mot_a_trouver, mot_propose):
    res = [None for _ in mot_propose]
    lettres_restantes = []
    for i, lettre in enumerate(mot_propose):
        if lettre == mot_a_trouver[i]:
            res[i] = "R"
        else:
            lettres_restantes.append(mot_a_trouver[i])

    for i, lettre in enumerate(mot_propose):
        if res[i] != "R" and lettre in lettres_restantes:
            res[i] = "Y"
            lettres_restantes.remove(lettre)

    return res


if __name__=="__main__":
    target_word = "MOTUO"
    guessed_word = "MOLOS"

    result_highlights = find_colors(target_word, guessed_word)
    print(result_highlights)